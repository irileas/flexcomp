package nodomain.irileas.flexcomp.parsepiler;

import nodomain.irileas.flexcomp.parsepiler.ast.expr.call.ASTCall;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.call.ASTMemberCall;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.call.ASTSuperCall;
import nodomain.irileas.flexcomp.parsepiler.error.SyntaxError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.*;
import nodomain.irileas.flexcomp.parsepiler.ast.*;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.constant.*;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.objaccess.*;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.op.ASTBinaryOp;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.op.ASTUnaryOp;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.*;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.control.ASTBreak;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.control.ASTContinue;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.control.ASTReturn;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.control.decision.ASTIf;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.control.decision.ASTSwitch;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.control.looping.ASTFor;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.control.looping.ASTObjectloop;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.control.looping.ASTWhile;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.objaccess.*;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.scope.ASTNewVariable;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.*;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Operator;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Symbol;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Token;
import nodomain.irileas.flexcomp.util.Pair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Parser {
    private final Tokenizer tokenizer;

    private ASTExpression parseSimpleExpression() throws IOException, SyntaxError {
        if (tokenizer.checkToken(Token.LEFT_BRACKET)) {
            return parseFunction();
        } else if (tokenizer.checkToken(Token.KW_OBJECT)) {
            return parseObject();
        } else if (tokenizer.checkToken(Token.LEFT_PAREN)) {
            ASTExpression expr = parseExpression();
            tokenizer.expectToken(Token.RIGHT_PAREN);
            return expr;
        } else if (tokenizer.checkToken(Token.KW_SELF)) {
            return new ASTSelf();
        } else if (tokenizer.checkToken(Token.KW_TRUE)) {
            return new ASTTrue();
        } else if (tokenizer.checkToken(Token.KW_FALSE)) {
            return new ASTFalse();
        } else if (tokenizer.checkToken(Token.KW_NIL)) {
            return new ASTNil();
        } else if (tokenizer.checkToken(Token.KW_NEW)) {
            Symbol name = tokenizer.checkSymbol();
            ASTExpression[] params = (tokenizer.checkToken(Token.LEFT_PAREN) ? parseParameterList() : new ASTExpression[0]);
            return new ASTNew(name, params);
        } else if (tokenizer.checkToken(Token.KW_SUPER)) {
            tokenizer.expectToken(Token.DOT);
            Symbol key = tokenizer.expectSymbol();
            return new ASTSuperCall(key, tokenizer.checkToken(Token.LEFT_PAREN) ? parseParameterList() : new ASTExpression[0]);
        } else {
            Symbol sym = tokenizer.checkSymbol();
            if (sym != null) return new ASTSymbol(sym);
            Token token = tokenizer.next();
            if (token instanceof ValueToken) {
                return ((ValueToken) token).asAST();
            } else {
                tokenizer.unread(token);
            }
            return null;
        }
    }

    private ASTExpression[] parseParameterList() throws IOException, SyntaxError {
        List<ASTExpression> exprs = new ArrayList<>();
        if (!tokenizer.checkToken(Token.RIGHT_PAREN)) {
            do {
                exprs.add(parseExpression());
            } while (tokenizer.checkToken(Token.COMMA));
            tokenizer.expectToken(Token.RIGHT_PAREN);
        }
        return exprs.toArray(new ASTExpression[]{});
    }

    private ASTExpression parseExpression(int operatorPrecedence) throws IOException, SyntaxError {
        ASTExpression lhs = parseSimpleExpression();

        if (lhs == null) {
            Operator op = tokenizer.checkOperator();
            if (op == null) return null;
            if (op.getUnaryPrecedence() != Operator.PRECEDENCE_INVALID) {
                lhs = new ASTUnaryOp(op, parseExpression(Math.min(operatorPrecedence, op.getUnaryPrecedence())));
            } else {
                tokenizer.unread(op);
                return null;
            }
        }

        while (true) {
            Operator op = tokenizer.checkOperator();
            if (op == null) {
                if (tokenizer.checkToken(Token.LEFT_PAREN)) {
                    lhs = new ASTCall(lhs, parseParameterList());
                    continue;
                } else {
                    return lhs;
                }
            }

            if (op.getBinaryPrecedence() <= operatorPrecedence) {
                if (op == Operator.DOT) {
                    Symbol key = tokenizer.expectSymbol();
                    if (tokenizer.checkToken(Token.LEFT_PAREN)) {
                        lhs = new ASTMemberCall(lhs, key, parseParameterList());
                    } else {
                        lhs = new ASTMemberAccess(lhs, key);
                    }
                } else if (op == Operator.KW_HAS) {
                    lhs = new ASTHas(lhs, tokenizer.expectSymbol());
                } else if (op == Operator.KW_HASNT) {
                    lhs = new ASTUnaryOp(Operator.LOGICAL_NOT, new ASTHas(lhs, tokenizer.expectSymbol()));
                } else if (op == Operator.KW_PROVIDES) {
                    lhs = new ASTProvides(lhs, tokenizer.expectSymbol());
                } else {
                    lhs = new ASTBinaryOp(op, lhs, parseExpression(op.getBinaryPrecedence()));
                }
            } else {
                tokenizer.unread(op);
                return lhs;
            }
        }
    }

    private ASTExpression parseExpression() throws IOException, SyntaxError {
        return parseExpression(Operator.PRECEDENCE_INVALID - 1);
    }

    private ASTBlock parseBlock(Token stop) throws IOException, SyntaxError {
        List<ASTInstruction> instructions = new ArrayList<>();

        while (!tokenizer.checkToken(stop)) {
            ASTInstruction instruction = parseInstruction();
            if (instruction != null) instructions.add(instruction);
        }

        return new ASTBlock(instructions.toArray(new ASTInstruction[]{}));
    }

    private ASTSwitch parseSwitch(ASTExpression head, Token stop) throws IOException, SyntaxError {
        ASTExpression currentCase = null;
        List<ASTInstruction> instructions = new ArrayList<>();
        List<Pair<ASTExpression, ASTInstruction[]>> cases = new ArrayList<>();

        while (!tokenizer.checkToken(stop)) {
            if (tokenizer.checkToken(Token.KW_CASE)) {
                do {
                    cases.add(new Pair(currentCase, instructions.toArray(new ASTInstruction[]{})));
                    instructions.clear();
                    currentCase = parseExpression();
                } while (tokenizer.checkToken(Token.COMMA));
                tokenizer.expectToken(Token.COLON);
            } else if (tokenizer.checkToken(Token.KW_DEFAULT)) {
                tokenizer.expectToken(Token.COLON);
                cases.add(new Pair(currentCase, instructions.toArray(new ASTInstruction[]{})));
                instructions.clear();
                currentCase = ASTSwitch.DEFAULT_EXPRESSION;
            } else {
                ASTInstruction instruction = parseInstruction();
                if (instruction != null) instructions.add(instruction);
            }
        }

        if (!instructions.isEmpty()) cases.add(new Pair(currentCase, instructions.toArray(new ASTInstruction[]{})));

        return new ASTSwitch(head, cases.toArray(new Pair[]{}));
    }

    private ASTInstruction parseInstruction(Token stop) throws IOException, SyntaxError {
        if (tokenizer.checkToken(Token.LEFT_CURLY)) {
            return parseBlock(Token.RIGHT_CURLY);
        } else if (tokenizer.checkToken(Token.KW_FOR)) {
            tokenizer.expectToken(Token.LEFT_PAREN);
            ASTInstruction init = parseInstruction(Token.COLON);
            ASTExpression condition = parseExpression();
            tokenizer.expectToken(Token.COLON);
            ASTInstruction update = parseInstruction(Token.RIGHT_PAREN);
            ASTInstruction body = parseInstruction();
            return new ASTFor(init, condition, update, body);
        } else if (tokenizer.checkToken(Token.KW_WHILE)) {
            tokenizer.expectToken(Token.LEFT_PAREN);
            ASTExpression condition = parseExpression();
            tokenizer.expectToken(Token.RIGHT_PAREN);
            ASTInstruction body = parseInstruction();
            return new ASTWhile(condition, body);
        } else if (tokenizer.checkToken(Token.KW_IF)) {
            tokenizer.expectToken(Token.LEFT_PAREN);
            ASTExpression condition = parseExpression();
            tokenizer.expectToken(Token.RIGHT_PAREN);
            ASTInstruction consequent = parseInstruction(null);
            ASTInstruction alternative = null;
            if (tokenizer.checkToken(Token.KW_ELSE)) {
                alternative = parseInstruction();
            }
            return new ASTIf(condition, consequent, alternative);
        } else if (tokenizer.checkToken(Token.KW_BREAK)) {
            tokenizer.expectToken(stop);
            return new ASTBreak();
        } else if (tokenizer.checkToken(Token.KW_CONTINUE)) {
            tokenizer.expectToken(stop);
            return new ASTContinue();
        } else if (tokenizer.checkToken(Token.KW_RETURN)) {
            ASTExpression retval = parseExpression();
            tokenizer.expectToken(stop);
            return new ASTReturn(retval);
        } else if (tokenizer.checkToken(Token.KW_DELETE)) {
            ASTExpression val = parseExpression();
            tokenizer.expectToken(stop);
            return new ASTDelete(val);
        } else if (tokenizer.checkToken(Token.KW_MOVE)) {
            ASTExpression obj = parseExpression();
            tokenizer.expectToken(Token.KW_TO);
            ASTExpression target = parseExpression();
            return new ASTMoveObjectTo(obj, target);
        } else if (tokenizer.checkToken(Operator.KW_GIVE)) {
            boolean value = !tokenizer.checkToken(Operator.LOGICAL_NOT);
            tokenizer.expectToken(Token.KW_TO);
            ASTExpression expr = parseExpression();
            tokenizer.expectToken(stop);
            return new ASTGive(expr, tokenizer.expectSymbol(), value);
        } else if (tokenizer.checkToken(Token.KW_REMOVE)) {
            if (tokenizer.checkToken(Token.KW_PROPERTY)) {
                Symbol flag = tokenizer.expectSymbol();
                tokenizer.expectToken(Token.KW_FROM);
                ASTExpression object = parseExpression();
                tokenizer.expectToken(stop);
                return new ASTRemoveProperty(object, flag);
            } else if (tokenizer.checkToken(Token.KW_FLAG)) {
                Symbol flag = tokenizer.expectSymbol();
                tokenizer.expectToken(Token.KW_FROM);
                ASTExpression object = parseExpression();
                tokenizer.expectToken(stop);
                return new ASTRemoveFlag(object, flag);
            } else {
                ASTExpression object = parseExpression();
                tokenizer.expectToken(stop);
                return new ASTMoveObjectTo(object, new ASTNil());
            }
        } else if (tokenizer.checkToken(Token.KW_LET)) {
            Symbol name = tokenizer.expectSymbol();
            ASTExpression value = null;
            if (tokenizer.checkToken(Token.ASSIGNMENT)) value = parseExpression();
            ASTNewVariable var = new ASTNewVariable(name, value);
            tokenizer.expectToken(stop);
            return var;
        } else if (tokenizer.checkToken(Token.KW_OBJECTLOOP)) {
            tokenizer.expectToken(Token.LEFT_PAREN);
            Symbol var = tokenizer.expectSymbol();
            tokenizer.expectToken(Token.COLON);     // This is a small derivation from Inform 6's objectloop syntax
            ASTExpression expression = parseExpression();
            tokenizer.expectToken(Token.RIGHT_PAREN);
            ASTInstruction body = parseInstruction();
            return new ASTObjectloop(var, expression, body);
        } else if (tokenizer.checkToken(Token.KW_SWITCH)) {
            tokenizer.expectToken(Token.LEFT_PAREN);
            ASTExpression switcher = parseExpression();
            tokenizer.expectToken(Token.RIGHT_PAREN);
            tokenizer.expectToken(Token.LEFT_CURLY);
            return parseSwitch(switcher, Token.RIGHT_CURLY);
        } else {
            ASTExpression expression = parseExpression();
            tokenizer.expectToken(stop);
            return expression;
        }
    }

    private ASTInstruction parseInstruction() throws IOException, SyntaxError {
        return parseInstruction(Token.SEMICOLON);
    }

    private ASTFunction parseFunction() throws IOException, SyntaxError {
        List<Symbol> params = new ArrayList<>();

        while (!tokenizer.checkToken(Token.SEMICOLON)) {
            params.add(tokenizer.expectSymbol());
        }

        return new ASTFunction(params.toArray(new Symbol[]{}), parseBlock(Token.RIGHT_BRACKET));
    }

    private ASTObject parseObject() throws IOException, SyntaxError {
        ASTObject object = new ASTObject();

        while (true) {
            Token mode = tokenizer.next();

            while (true) {
                if (mode == Token.KW_WITH) {
                    Symbol attrName = tokenizer.checkSymbol();
                    if (attrName == null) break;
                    tokenizer.checkToken(Token.ASSIGNMENT); // Writing "x = 5" is legal, too!
                    ASTExpression value = parseExpression();
                    object.addAttribute(attrName, value);
                    if (!tokenizer.checkToken(Token.COMMA)) break;
                } else if (mode == Token.KW_HAS) {
                    boolean value = !tokenizer.checkToken(Token.LOGICAL_NOT);
                    Symbol name = tokenizer.checkSymbol();
                    if (name != null) object.addFlag(name, value);
                    else if (tokenizer.checkToken(Token.COMMA)) break;  // A comma ends this list
                    else break;
                } else if (mode == Token.KW_CLASS) {
                    Symbol parentName = tokenizer.checkSymbol();
                    if (parentName != null) object.inherit(parentName);
                    else if (tokenizer.checkToken(Token.COMMA)) break;  // A comma ends this list
                    else break;
                } else if (mode == Token.KW_IN) {
                    object.locate(tokenizer.expectSymbol());
                    tokenizer.checkToken(Token.COMMA);
                    break;
                } else if (mode == Token.SEMICOLON) {
                    tokenizer.unread(Token.SEMICOLON);
                    return object;
                } else {
                    throw new SyntaxError("Expected WITH, HAS, CLASS or SEMICOLON!");
                }
            }
        }
    }

    public ASTUnit parse() throws IOException, SyntaxError {
        ASTUnit unit = new ASTUnit();

        while (tokenizer.checkToken(Token.KW_USES)) {
            do {
                unit.uses(tokenizer.expectSymbol());
            } while (tokenizer.checkToken(Token.COMMA));
            tokenizer.expectToken(Token.SEMICOLON);
        }

        while (!tokenizer.checkToken(Token.EOF)) {
            Token token = tokenizer.next();
            Symbol name = tokenizer.expectSymbol();

            if (token == Token.KW_OBJECT) {
                unit.addDecl(name, parseObject());
            } else if (token == Token.KW_VARIABLE) {
                unit.addDecl(name, tokenizer.checkToken(Token.ASSIGNMENT) ? parseExpression() : null);
            } else if (token == Token.LEFT_BRACKET) {
                unit.addDecl(name, parseFunction());
            } else {
                if (token instanceof Symbol) {
                    ASTObject object = parseObject();
                    object.inherit((Symbol) token);
                    unit.addDecl(name, object);
                } else {
                    throw new SyntaxError("Expected OBJECT, VARIABLE, LEFT_BRACKET or a class name!");
                }
            }

            tokenizer.expectToken(Token.SEMICOLON);
        }

        return unit;
    }

    public Parser(Tokenizer tokenizer) {
        this.tokenizer = tokenizer;
    }
}
