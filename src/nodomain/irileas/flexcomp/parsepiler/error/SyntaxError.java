package nodomain.irileas.flexcomp.parsepiler.error;

public class SyntaxError extends Throwable {
    public SyntaxError(String msg) {
        super(msg);
    }
}
