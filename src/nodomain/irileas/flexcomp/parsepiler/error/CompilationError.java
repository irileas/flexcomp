package nodomain.irileas.flexcomp.parsepiler.error;

import nodomain.irileas.flexcomp.parsepiler.ast.AST;

public class CompilationError extends Throwable {
    private final AST ast;

    public AST getAST() {
        return ast;
    }

    public CompilationError(AST ast, String msg) {
        super(msg);
        this.ast = ast;
    }
}
