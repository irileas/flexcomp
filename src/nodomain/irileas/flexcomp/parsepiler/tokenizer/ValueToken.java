package nodomain.irileas.flexcomp.parsepiler.tokenizer;

import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Token;

public abstract class ValueToken extends Token {
    public abstract ASTExpression asAST();
}
