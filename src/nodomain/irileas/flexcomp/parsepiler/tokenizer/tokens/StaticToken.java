package nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens;

import java.util.HashMap;
import java.util.Map;

public class StaticToken extends Token {
    private final String name;

    @Override
    public String toString() {
        return "Token:" + name;
    }

    public StaticToken(String name) {
        this.name = name;
        staticTokens.put(name, this);
    }

    private static Map<String, StaticToken> staticTokens = new HashMap<>();
    public static Token get(String name) {
        return staticTokens.get(name);
    }
}
