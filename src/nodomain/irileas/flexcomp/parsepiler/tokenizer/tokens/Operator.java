package nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens;

public class Operator extends StaticToken {
    public static final int PRECEDENCE_INVALID = Integer.MAX_VALUE;

    private final int unaryPrecedence;
    private final int binaryPrecedence;

    public Operator(String name, int unaryPrecedence, int binaryPrecedence) {
        super(name);
        this.unaryPrecedence = unaryPrecedence;
        this.binaryPrecedence = binaryPrecedence;
    }

    public int getUnaryPrecedence() {
        return unaryPrecedence;
    }

    public int getBinaryPrecedence() {
        return binaryPrecedence;
    }
}
