package nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens;

import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.constant.ASTString;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.ValueToken;

public class StringToken extends ValueToken {
    private final String value;

    @Override
    public ASTExpression asAST() {
        return new ASTString(value);
    }

    public StringToken(String value) {
        this.value = value;
    }
}
