package nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens;

import java.util.HashMap;
import java.util.Map;

public class Symbol extends Token {
    private String name;

    public String getName() { return name; }

    @Override
    public String toString() {
        return "#" + name;
    }

    private Symbol(String name) {
        this.name = name;
    }

    private static final Map<String, Symbol> symbols = new HashMap<>();
    public static Symbol get(String name) {
        if (symbols.containsKey(name)) {
            return symbols.get(name);
        } else {
            Symbol sym = new Symbol(name);
            symbols.put(name, sym);
            return sym;
        }
    }
    private static int GENSYM_COUNTER = 0;
    public static Symbol gensym() {
        return new Symbol("#:gensym" + GENSYM_COUNTER++);
    }
}
