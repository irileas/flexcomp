package nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens;

import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.constant.ASTInteger;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.ValueToken;

public class IntegerToken extends ValueToken {
    private final int value;

    public int getValue() {
        return value;
    }

    @Override
    public ASTExpression asAST() {
        return new ASTInteger(value);
    }

    public IntegerToken(int value) {
        super();
        this.value = value;
    }
}
