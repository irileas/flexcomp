package nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens;

public abstract class Token {
    public static final Token EOF = new StaticToken(" EOF");

    public static final Token LEFT_PAREN = new StaticToken(" LPAREN");
    public static final Token RIGHT_PAREN = new StaticToken(" RPAREN");
    public static final Token LEFT_BRACKET = new StaticToken(" LBRACKET");
    public static final Token RIGHT_BRACKET = new StaticToken(" RBRACKET");
    public static final Token LEFT_CURLY = new StaticToken(" LCURLY");
    public static final Token RIGHT_CURLY = new StaticToken(" RCURLY");
    public static final Token SEMICOLON = new StaticToken(" SEMICOLON");
    public static final Token COLON = new StaticToken(" COLON");
    public static final Token COMMA = new StaticToken(" COMMA");

    public static final Token DOT = new Operator(" OP_DOT", Operator.PRECEDENCE_INVALID, 1);
    public static final Token ASSIGNMENT = new Operator(" OP_ASSIGNMENT", Operator.PRECEDENCE_INVALID, 14);
    public static final Token COMPARISON_EQ = new Operator(" OP_COMPARISON_EQ", Operator.PRECEDENCE_INVALID, 7);
    public static final Token COMPARISON_INEQ = new Operator(" OP_COMPARISON_INEQ", Operator.PRECEDENCE_INVALID, 7);
    public static final Token COMPARISON_LESS = new Operator(" OP_COMPARISON_LESS", Operator.PRECEDENCE_INVALID, 6);
    public static final Token COMPARISON_GREATER = new Operator(" OP_COMPARISON_GREATER", Operator.PRECEDENCE_INVALID, 6);
    public static final Token COMPARISON_LEQ = new Operator(" OP_COMPARISON_LEQ", Operator.PRECEDENCE_INVALID, 6);
    public static final Token COMPARISON_GEQ = new Operator(" OP_COMPARISON_GEQ", Operator.PRECEDENCE_INVALID, 6);
    public static final Operator LOGICAL_NOT = new Operator("not", 2, Operator.PRECEDENCE_INVALID);
    public static final Token LOGICAL_AND = new Operator("and", Operator.PRECEDENCE_INVALID, 11);
    public static final Token LOGICAL_OR = new Operator("or", Operator.PRECEDENCE_INVALID, 12);
    public static final Token MATHS_PLUS = new Operator(" OP_MATHS_PLUS", 3, 4);
    public static final Token MATHS_MINUS = new Operator(" OP_MATHS_MINUS", 3, 4);
    public static final Token MATHS_TIMES = new Operator(" OP_MATHS_TIMES", Operator.PRECEDENCE_INVALID, 3);
    public static final Token MATHS_DIV = new Operator(" OP_MATHS_DIV", Operator.PRECEDENCE_INVALID, 3);
    public static final Token MATHS_MOD = new Operator("mod", Operator.PRECEDENCE_INVALID, 3);

    public static final Token KW_CHILD = new Operator("child", 4, Operator.PRECEDENCE_INVALID);
    public static final Token KW_SIBLING = new Operator("parent", 4, Operator.PRECEDENCE_INVALID);
    public static final Token KW_PARENT = new Operator("sibling", 4, Operator.PRECEDENCE_INVALID);

    public static final Token KW_IS = new Operator("is", Operator.PRECEDENCE_INVALID, 2);
    public static final Token KW_IN = new Operator("in", Operator.PRECEDENCE_INVALID, 2);
    public static final Token KW_HAS = new Operator("has", Operator.PRECEDENCE_INVALID, 2);
    public static final Token KW_HASNT = new Operator("hasnt", Operator.PRECEDENCE_INVALID, 2);
    public static final Token KW_PROVIDES = new Operator("provides", Operator.PRECEDENCE_INVALID, 2);
    public static final Token KW_GIVE = new StaticToken("give");
    public static final Token KW_REMOVE = new StaticToken("remove");
    public static final Token KW_FLAG = new StaticToken("flag");
    public static final Token KW_PROPERTY = new StaticToken("property");

    public static final Token KW_UNIT = new StaticToken("unit");
    public static final Token KW_USES = new StaticToken("uses");
    public static final Token KW_VARIABLE = new StaticToken("variable");
    public static final Token KW_OBJECT = new StaticToken("object");
    public static final Token KW_CLASS = new StaticToken("class");
    public static final Token KW_WITH = new StaticToken("with");
    public static final Token KW_NEW = new StaticToken("new");
    public static final Token KW_DELETE = new StaticToken("delete");

    public static final Token KW_SELF = new StaticToken("self");
    public static final Token KW_SUPER = new StaticToken("super");
    public static final Token KW_TRUE = new StaticToken("true");
    public static final Token KW_FALSE = new StaticToken("false");
    public static final Token KW_NIL = new StaticToken("nil");

    public static final Token KW_LET = new StaticToken("let");
    public static final Token KW_OBJECTLOOP = new StaticToken("objectloop");
    public static final Token KW_FOR = new StaticToken("for");
    public static final Token KW_WHILE = new StaticToken("while");
    public static final Token KW_BREAK = new StaticToken("break");
    public static final Token KW_CONTINUE = new StaticToken("continue");
    public static final Token KW_IF = new StaticToken("if");
    public static final Token KW_ELSE = new StaticToken("else");
    public static final Token KW_RETURN = new StaticToken("return");
    public static final Token KW_MOVE = new StaticToken("move");
    public static final Token KW_TO = new StaticToken("to");
    public static final Token KW_FROM = new StaticToken("from");
    public static final Token KW_SWITCH = new StaticToken("switch");
    public static final Token KW_CASE = new StaticToken("case");
    public static final Token KW_DEFAULT = new StaticToken("default");
}
