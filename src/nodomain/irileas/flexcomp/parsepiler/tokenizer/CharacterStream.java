package nodomain.irileas.flexcomp.parsepiler.tokenizer;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Stack;

public class CharacterStream {
    private final Reader stream;
    private Stack<Integer> pushback = new Stack<>();

    public void unread(int c) {
        if (c >= 0) pushback.push(c);
    }

    public int read() throws IOException {
        if (!pushback.isEmpty()) return pushback.pop();
        else return stream.read();
    }

    public CharacterStream(InputStreamReader reader) {
        this.stream = reader;
    }
}
