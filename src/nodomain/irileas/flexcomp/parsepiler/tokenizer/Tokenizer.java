package nodomain.irileas.flexcomp.parsepiler.tokenizer;

import nodomain.irileas.flexcomp.parsepiler.error.SyntaxError;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Stack;

public class Tokenizer {
    private final CharacterStream stream;
    private final Stack<Token> pushbacks = new Stack();

    private boolean checkChar(char check) throws IOException {
        int c = stream.read();
        if (c == check) return true;
        stream.unread(c);
        return false;
    }

    private void slurpWhitespace() throws IOException {
        int c;
        while (Character.isWhitespace(c = stream.read()));
        stream.unread(c);
    }

    private boolean isIdentifierChar(int c) {
        return (Character.isAlphabetic(c) || Character.isDigit(c) || c == '_' || c == '$'); // TODO: More characters!
    }

    private String parseAlphanumericIdentifier() throws IOException {
        StringBuilder builder = new StringBuilder();
        int c;
        while (isIdentifierChar(c = stream.read())) {
            builder.append((char) c);
        }
        stream.unread(c);
        return builder.toString();
    }

    private int frombase(char c, int base) {
        if (c >= '0' && c < ('0' + Math.min(base, 10))) return c - '0';
        else if (c >= 'A' && c < ('A' + base - 10)) return c - 'A' + 10;
        else if (c >= 'a' && c < ('a' + base - 10)) return c - 'a' + 10;
        else return -1;
    }

    private final String[] lettertable = {
            "*?¿",
            "*!¡",
            "*sß",
            "*Sẞ",
            ":aä",
            ":AÄ",
            ":eë",
            ":EË",
            ":iï",
            ":IÏ",
            ":oö",
            ":OÖ",
            ":uü",
            ":UÜ",
            "^aâ",
            "^eê",
            "^iî",
            "^oô",
            "^uû",
            "^AÂ",
            "^EÊ",
            "^IÎ",
            "^OÔ",
            "^UÛ",
            "/oø",
            "/OØ",
            "/lł",
            "/LŁ",
    };
    private char parseChar() throws IOException, SyntaxError {
        int c = stream.read();
        if (c == '\\') {
            int code = stream.read();
            switch (code) {
                case -1:
                    break;
                case '\\': return '\\';
                case 't':  return '\t';
                case 'n':  return '\n';
                case 'r':  return '\r';
                case 'u':
                case 'U': {
                    int num = 0;
                    for (int counter = 0; counter < 4; counter++) {
                        num = num * 16 + frombase((char) stream.read(), 16);
                    }
                    return (char) num;
                }
                default: {
                    c = stream.read();
                    for (String s : lettertable) {
                        if (code == s.charAt(0) && c == s.charAt(1)) {
                            return s.charAt(2);
                        }
                    }
                    throw new SyntaxError("Unknown character escape macro: \\" + code);
                }
            }
        }
        return (char) c;
    }

    private String parseString() throws IOException, SyntaxError {
        StringBuilder builder = new StringBuilder();
        while (true) {
            int c = stream.read();
            if (c == '\"' || c == -1) break;
            stream.unread(c);
            builder.append(parseChar());
        }
        return builder.toString();
    }

    private int parseInteger(int base) throws IOException {
        int i = 0;
        int c;
        do {
            c = stream.read();
            if (c < 0) return i;
            int v = frombase((char) c, base);
            if (v < 0) break;
            i = i * base + v;
        } while (true);
        stream.unread(c);
        return i;
    }

    public Token next() throws IOException, SyntaxError {
        if (!pushbacks.isEmpty()) return pushbacks.pop();

        slurpWhitespace();
        int c = stream.read();

        switch (c) {
            case -1: return Token.EOF;

            case '(': return Token.LEFT_PAREN;
            case ')': return Token.RIGHT_PAREN;
            case '[': return Token.LEFT_BRACKET;
            case ']': return Token.RIGHT_BRACKET;
            case '{': return Token.LEFT_CURLY;
            case '}': return Token.RIGHT_CURLY;
            case ';': return Token.SEMICOLON;
            case ':': return Token.COLON;
            case ',': return Token.COMMA;

            case '.': return Token.DOT;
            case '=': {
                if (checkChar('=')) return Token.COMPARISON_EQ;
                else return Token.ASSIGNMENT;
            }
            case '!': {
                if (checkChar('=')) return Token.COMPARISON_INEQ;
                else return Token.LOGICAL_NOT;
            }
            case '<': {
                if (checkChar('=')) return Token.COMPARISON_LEQ;
                else return Token.COMPARISON_LESS;
            }
            case '>': {
                if (checkChar('=')) return Token.COMPARISON_GEQ;
                else return Token.COMPARISON_GREATER;
            }
            case '&': {
                checkChar('&');
                return Token.LOGICAL_AND;
            }
            case '|': {
                checkChar('|');
                return Token.LOGICAL_OR;
            }
            case '+': return Token.MATHS_PLUS;
            case '-': return Token.MATHS_MINUS;
            case '*': return Token.MATHS_TIMES;
            case '/': return Token.MATHS_DIV;
            case '%': return Token.MATHS_MOD;

            case '#': {
                int prefix = parseInteger(10);
                switch (stream.read()) {
                    case 'b': return new IntegerToken(parseInteger(2));
                    case 'o': return new IntegerToken(parseInteger(8));
                    case 'x': return new IntegerToken(parseInteger(16));
                    case 'r': return new IntegerToken(parseInteger(prefix));
                    default: {
                        throw new SyntaxError("Expected a proper hash dispatch character!");
                    }
                }
            }

            case '$': {
                return new IntegerToken(parseChar());
            }

            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9': {
                stream.unread(c);
                return new IntegerToken(parseInteger(10));
            }
            case '\"': {
                return new StringToken(parseString());
            }
            default: {
                stream.unread(c);
                String text = parseAlphanumericIdentifier();
                if (text.isEmpty()) return Token.EOF;
                Token token = StaticToken.get(text.toLowerCase());
                if (token == null) return Symbol.get(text);
                else return token;
            }
        }
    }

    public boolean checkToken(Token check) throws IOException, SyntaxError {
        if (check == null) return true;
        Token token = next();
        if (token == check) return true;
        unread(token);
        return false;
    }

    public void expectToken(Token token) throws IOException, SyntaxError {
        if (!checkToken(token)) throw new RuntimeException("Syntax error! Expected token: " + token + "!");
    }

    public Symbol checkSymbol() throws IOException, SyntaxError {
        Token token = next();
        if (token instanceof Symbol) return (Symbol) token;
        unread(token);
        return null;
    }

    public Symbol expectSymbol() throws IOException, SyntaxError {
        Symbol symbol = checkSymbol();
        if (symbol == null) throw new RuntimeException("Syntax error! Expected symbol!");
        return symbol;
    }

    public Operator checkOperator() throws IOException, SyntaxError {
        Token token = next();
        if (token instanceof Operator) return (Operator) token;
        unread(token);
        return null;
    }

    public void unread(Token token) {
        pushbacks.push(token);
    }

    public Tokenizer(InputStream stream) throws UnsupportedEncodingException {
        this.stream = new CharacterStream(new InputStreamReader(stream, "UTF-8"));
    }
}
