package nodomain.irileas.flexcomp.parsepiler.ast.instr;

import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;

public class ASTBlock extends ASTInstruction {
    private final ASTInstruction[] instructions;

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        scope = scope.createSubscope();
        if (instructions.length > 0) {
            for (ASTInstruction instruction : instructions) {
                instruction.compile(compiler, scope);
            }
        } else {
            compiler.compileLoadSelf();
        }
        scope.finish();
    }

    public ASTBlock(ASTInstruction[] instructions) {
        this.instructions = instructions;
    }
}
