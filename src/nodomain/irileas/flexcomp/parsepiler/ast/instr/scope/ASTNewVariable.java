package nodomain.irileas.flexcomp.parsepiler.ast.instr.scope;

import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.ASTInstruction;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Symbol;

public class ASTNewVariable extends ASTInstruction {
    private final Symbol name;
    private final ASTExpression value;

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        scope.addVariable(name);
        if (value != null) {
            value.compile(compiler, scope);
            compiler.compileStoreLocal(scope.findVariable(name));
        }
    }

    public ASTNewVariable(Symbol name, ASTExpression value) {
        this.name = name;
        this.value = value;
    }
}
