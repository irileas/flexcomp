package nodomain.irileas.flexcomp.parsepiler.ast.instr.objaccess;

import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.ASTInstruction;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Symbol;

public class ASTGive extends ASTInstruction {
    private final ASTExpression expr;
    private final Symbol key;
    private final boolean value;

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        expr.compile(compiler, scope);
        if (value) compiler.compileGiveFlag(compiler.getBackend().createSymbol(key));
        else compiler.compileGiveNegatedFlag(compiler.getBackend().createSymbol(key));
    }

    public ASTGive(ASTExpression expr, Symbol key, boolean value) {
        this.expr = expr;
        this.key = key;
        this.value = value;
    }
}
