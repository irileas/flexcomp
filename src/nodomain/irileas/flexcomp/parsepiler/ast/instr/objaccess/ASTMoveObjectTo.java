package nodomain.irileas.flexcomp.parsepiler.ast.instr.objaccess;

import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.ASTInstruction;

public class ASTMoveObjectTo extends ASTInstruction {
    private final ASTExpression object;
    private final ASTExpression target;

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        object.compile(compiler, scope);
        compiler.compilePush();
        target.compile(compiler, scope);
        compiler.compileMoveTo();
    }

    public ASTMoveObjectTo(ASTExpression object, ASTExpression target) {
        this.object = object;
        this.target = target;
    }
}
