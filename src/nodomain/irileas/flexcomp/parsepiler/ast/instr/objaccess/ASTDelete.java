package nodomain.irileas.flexcomp.parsepiler.ast.instr.objaccess;

import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.ASTInstruction;

public class ASTDelete extends ASTInstruction {
    private ASTExpression object;

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        object.compile(compiler, scope);
        compiler.compileDelete();
    }

    public ASTDelete(ASTExpression object) {
        this.object = object;
    }
}
