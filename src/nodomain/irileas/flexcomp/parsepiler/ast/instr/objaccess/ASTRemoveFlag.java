package nodomain.irileas.flexcomp.parsepiler.ast.instr.objaccess;

import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.ASTInstruction;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Symbol;

public class ASTRemoveFlag extends ASTInstruction {
    private final ASTExpression object;
    private final Symbol flag;

    public ASTRemoveFlag(ASTExpression object, Symbol flag) {
        this.object = object;
        this.flag = flag;
    }

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        object.compile(compiler, scope);
        compiler.compileRemoveFlag(compiler.getBackend().createSymbol(flag));
    }
}
