package nodomain.irileas.flexcomp.parsepiler.ast.instr.objaccess;

import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.ASTInstruction;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Symbol;

public class ASTRemoveProperty extends ASTInstruction {
    private final ASTExpression object;
    private final Symbol property;

    public ASTRemoveProperty(ASTExpression object, Symbol flag) {
        this.object = object;
        this.property = flag;
    }

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        object.compile(compiler, scope);
        compiler.compileRemoveMember(compiler.getBackend().createSymbol(property));
    }
}
