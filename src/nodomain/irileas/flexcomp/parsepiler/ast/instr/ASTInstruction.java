package nodomain.irileas.flexcomp.parsepiler.ast.instr;

import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.AST;

public abstract class ASTInstruction extends AST {
    public abstract void compile(FunctionCompiler compiler, Scope scope) throws CompilationError;
}
