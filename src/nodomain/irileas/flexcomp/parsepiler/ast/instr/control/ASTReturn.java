package nodomain.irileas.flexcomp.parsepiler.ast.instr.control;

import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.ASTInstruction;

public class ASTReturn extends ASTInstruction {
    private ASTExpression returnValue;

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        if (returnValue == null) compiler.compileLoadSelf();
        else returnValue.compile(compiler, scope);
        compiler.compileReturn();
    }

    public ASTReturn(ASTExpression returnValue) {
        this.returnValue = returnValue;
    }
}
