package nodomain.irileas.flexcomp.parsepiler.ast.instr.control;

import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.JumpLabel;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.ASTInstruction;

public class ASTBreak extends ASTInstruction {
    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        JumpLabel label = scope.getBreakLabel();
        if (label == null) throw new CompilationError(this, "Not in a loop!");
        else label.compileJump();
    }
}
