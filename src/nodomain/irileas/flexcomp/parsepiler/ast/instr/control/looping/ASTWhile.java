package nodomain.irileas.flexcomp.parsepiler.ast.instr.control.looping;

import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.ASTInstruction;

public class ASTWhile extends ASTFor {
    public ASTWhile(ASTExpression condition, ASTInstruction body) {
        super(null, condition, null, body);
    }
}
