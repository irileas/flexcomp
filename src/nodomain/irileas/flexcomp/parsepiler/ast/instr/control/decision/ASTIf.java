package nodomain.irileas.flexcomp.parsepiler.ast.instr.control.decision;

import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.JumpLabel;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.constant.ASTSelf;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.ASTInstruction;

public class ASTIf extends ASTInstruction {
    private final ASTExpression condition;
    private final ASTInstruction consequent;
    private final ASTInstruction alternative;

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        condition.compile(compiler, scope);

        JumpLabel targetInCaseOfFalse = compiler.openLabel();
        targetInCaseOfFalse.compileJumpIfNot();
        if (consequent != null) consequent.compile(compiler, scope);

        if (alternative != null) {
            JumpLabel end = compiler.openLabel();
            end.compileJump();
            targetInCaseOfFalse.place();
            alternative.compile(compiler, scope);
            end.place();
        } else {
            targetInCaseOfFalse.place();
        }
    }

    public ASTIf(ASTExpression condition, ASTInstruction consequent, ASTInstruction alternative) {
        super();
        this.condition = condition;
        this.consequent = consequent;
        this.alternative = alternative;
    }
}
