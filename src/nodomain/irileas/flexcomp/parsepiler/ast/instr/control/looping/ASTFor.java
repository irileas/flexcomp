package nodomain.irileas.flexcomp.parsepiler.ast.instr.control.looping;

import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.JumpLabel;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.ASTInstruction;

public class ASTFor extends ASTInstruction {
    private final ASTInstruction init;
    private final ASTExpression condition;
    private final ASTInstruction update;
    private final ASTInstruction body;

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        JumpLabel headLabel = compiler.openLabel();
        JumpLabel breakLabel = compiler.openLabel();
        JumpLabel continueLabel = compiler.openLabel();
        scope = scope.createSubscope(breakLabel, continueLabel);

        if (init != null) init.compile(compiler, scope);

        headLabel.place();  // The loop body begins here
        if (update == null) continueLabel.place();  // Optimized loop: Jump directly to the beginning if there's no update!
        if (condition != null) {
            condition.compile(compiler, scope);
            breakLabel.compileJumpIfNot();  // Jump to end of loop
        }
        if (body != null) body.compile(compiler, scope);
        if (update != null) {
            continueLabel.place();
            update.compile(compiler, scope);
        }
        headLabel.compileJump();
        breakLabel.place();
        scope.finish();
    }

    public ASTFor(ASTInstruction init, ASTExpression condition, ASTInstruction update, ASTInstruction body) {
        super();
        this.init = init;
        this.condition = condition;
        this.update = update;
        this.body = body;
    }
}
