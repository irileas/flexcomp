package nodomain.irileas.flexcomp.parsepiler.ast.instr.control.looping;

import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.JumpLabel;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.ASTInstruction;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Symbol;

public class ASTObjectloop extends ASTInstruction {
    private final Symbol var;
    private final ASTExpression condition;
    private final ASTInstruction body;

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        JumpLabel breakLabel = compiler.openLabel();
        JumpLabel continueLabel = compiler.openLabel();

        scope = scope.createSubscope(breakLabel, continueLabel);   // TODO: Break and continue?
        scope.addVariable(var);
        final int varLocation = scope.findVariable(var);
        Symbol nextObject = Symbol.gensym();
        scope.addVariable(nextObject);
        final int nextObjectVarLocation = scope.findVariable(nextObject);

        // TODO: Optimize! E.g. if we're only looking for objects of type X inside of object Y...

        compiler.compileLoadFirstObject();
        compiler.compileStoreLocal(nextObjectVarLocation);

        continueLabel.place();
        compiler.compileLoadLocal(nextObjectVarLocation);
        breakLabel.compileJumpIfNot();
        compiler.compilePush();
        compiler.compileLoadNextObject();
        compiler.compileStoreLocal(nextObjectVarLocation);
        compiler.compileStoreLocal(varLocation);
        condition.compile(compiler, scope);
        continueLabel.compileJumpIfNot();
        body.compile(compiler, scope);
        continueLabel.compileJump();
        breakLabel.place();
    }

    public ASTObjectloop(Symbol var, ASTExpression condition, ASTInstruction body) {
        super();
        this.var = var;
        this.condition = condition;
        this.body = body;
    }
}
