package nodomain.irileas.flexcomp.parsepiler.ast.instr.control.decision;

import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.JumpLabel;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.ASTInstruction;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Symbol;
import nodomain.irileas.flexcomp.util.Pair;

import java.util.HashMap;
import java.util.Map;

public class ASTSwitch extends ASTInstruction {
    public static final ASTExpression DEFAULT_EXPRESSION = new ASTExpression() {
        @Override
        public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
            throw new CompilationError(this, "Internal helper expressions can't be compiled!");
        }
    };

    private final ASTExpression head;
    private final Pair<ASTExpression, ASTInstruction[]>[] cases;

    public ASTSwitch(ASTExpression head, Pair<ASTExpression, ASTInstruction[]>[] cases) {
        this.head = head;
        this.cases = cases;
    }

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        JumpLabel breakLabel = compiler.openLabel();
        JumpLabel defaultLabel = compiler.openLabel();

        scope = scope.createSubscope(breakLabel, null); // TODO: Labels

        Symbol symbol = Symbol.gensym();
        scope.addVariable(symbol);
        int pos = scope.findVariable(symbol);

        head.compile(compiler, scope);
        compiler.compileStoreLocal(scope.findVariable(symbol));

        Map<ASTExpression, JumpLabel> jumps = new HashMap<>();
        for (Pair<ASTExpression, ASTInstruction[]> aCase : cases) {
            if (aCase.getFirst() == DEFAULT_EXPRESSION) {
                jumps.put(aCase.getFirst(), defaultLabel);
            } else if (aCase.getFirst() != null) {
                compiler.compileLoadLocal(pos);
                compiler.compilePush();
                aCase.getFirst().compile(compiler, scope);
                compiler.compileCheckEQ();
                JumpLabel label = compiler.openLabel();
                label.compileJumpIf();
                jumps.put(aCase.getFirst(), label);
            }
        }
        if (jumps.containsKey(DEFAULT_EXPRESSION)) defaultLabel.compileJump();
        else breakLabel.compileJump();

        for (Pair<ASTExpression, ASTInstruction[]> aCase : cases) {
            JumpLabel jump = jumps.get(aCase.getFirst());
            if (jump != null) jump.place();
            for (ASTInstruction instr : aCase.getSecond()) {
                instr.compile(compiler, scope);
            }
        }

        breakLabel.place();
    }
}
