package nodomain.irileas.flexcomp.parsepiler.ast;

import nodomain.irileas.flexcomp.backend.Backend;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Symbol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ASTUnit extends AST {
    private List<Symbol> usedUnits = new ArrayList<>();
    private Map<Symbol, ASTExpression> decls = new HashMap<>();

    public void uses(Symbol unit) {
        if (!usedUnits.contains(unit)) usedUnits.add(unit);
    }

    public Symbol[] getUsedUnits() {
        return usedUnits.toArray(new Symbol[]{});
    }

    public void addDecl(Symbol name, ASTExpression value) {
        decls.put(name, value);
    }

    public void compile(Backend backend) throws CompilationError {
        for (Symbol key : decls.keySet()) {
            backend.bind(key, decls.get(key).precalculateValue(backend));
        }
    }
}
