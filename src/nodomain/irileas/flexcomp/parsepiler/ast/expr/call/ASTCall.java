package nodomain.irileas.flexcomp.parsepiler.ast.expr.call;

import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;

public class ASTCall extends ASTExpression {
    private final ASTExpression target;
    private final ASTExpression[] params;

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        for (ASTExpression expr : params) {
            expr.compile(compiler, scope);
            compiler.compilePushOnParamStack();
        }
        target.compile(compiler, scope);
        compiler.compileCall(params.length);
    }

    public ASTCall(ASTExpression target, ASTExpression[] params) {
        this.target = target;
        this.params = params;
    }
}
