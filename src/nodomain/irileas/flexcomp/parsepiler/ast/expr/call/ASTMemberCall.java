package nodomain.irileas.flexcomp.parsepiler.ast.expr.call;

import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Symbol;

public class ASTMemberCall extends ASTExpression {
    private final ASTExpression target;
    private final Symbol key;
    private final ASTExpression[] params;

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        for (ASTExpression expr : params) {
            expr.compile(compiler, scope);
            compiler.compilePushOnParamStack();
        }
        target.compile(compiler, scope);
        compiler.compileCallMember(compiler.getBackend().createSymbol(key), params.length);
    }

    public ASTMemberCall(ASTExpression target, Symbol key, ASTExpression[] params) {
        this.target = target;
        this.key = key;
        this.params = params;
    }
}
