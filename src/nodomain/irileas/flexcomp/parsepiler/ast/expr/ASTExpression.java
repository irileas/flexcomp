package nodomain.irileas.flexcomp.parsepiler.ast.expr;

import nodomain.irileas.flexcomp.backend.Backend;
import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.ASTInstruction;

public abstract class ASTExpression extends ASTInstruction {

    public int precalculateValue(Backend backend) throws CompilationError {
        throw new CompilationError(this, "Can't convert expression to a value!");
    }

    public void compileAssignment(FunctionCompiler compiler, Scope scope) throws CompilationError {
        throw new CompilationError(this, "Can't assign to this expression!");
    }
}
