package nodomain.irileas.flexcomp.parsepiler.ast.expr.op;

import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.JumpLabel;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Operator;

public class ASTBinaryOp extends ASTExpression {
    private final Operator operator;
    private final ASTExpression lhs;
    private final ASTExpression rhs;

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        if (operator == Operator.ASSIGNMENT) {
            rhs.compile(compiler, scope);
            compiler.compilePush();
            lhs.compileAssignment(compiler, scope);
            return;
        } else if (operator == Operator.LOGICAL_OR) {
            JumpLabel skipper = compiler.openLabel();
            lhs.compile(compiler, scope);
            skipper.compileJumpIf();
            rhs.compile(compiler, scope);
            skipper.place();
            return;
        } else if (operator == Operator.LOGICAL_AND) {
            JumpLabel skipper = compiler.openLabel();
            JumpLabel end = compiler.openLabel();
            lhs.compile(compiler, scope);
            skipper.compileJumpIfNot();
            rhs.compile(compiler, scope);
            skipper.compileJumpIfNot();
            end.compileJump();
            skipper.place();
            compiler.compileLoadFalse();
            end.place();
            return;
        }

        lhs.compile(compiler, scope);
        compiler.compilePush();
        rhs.compile(compiler, scope);

        if (operator == Operator.COMPARISON_EQ) compiler.compileCheckEQ();
        else if (operator == Operator.COMPARISON_INEQ) compiler.compileCheckINEQ();
        else if (operator == Operator.COMPARISON_LESS) compiler.compileCheckLESS();
        else if (operator == Operator.COMPARISON_LEQ) compiler.compileCheckLEQ();
        else if (operator == Operator.COMPARISON_GREATER) compiler.compileCheckGREATER();
        else if (operator == Operator.COMPARISON_GEQ) compiler.compileCheckGEQ();
        else if (operator == Operator.MATHS_PLUS) compiler.compileAddition();
        else if (operator == Operator.MATHS_MINUS) compiler.compileSubtraction();
        else if (operator == Operator.MATHS_TIMES) compiler.compileMultiplication();
        else if (operator == Operator.MATHS_DIV) compiler.compileDivision();
        else if (operator == Operator.MATHS_MOD) compiler.compileModulo();
        else if (operator == Operator.KW_IS) compiler.compileIs();
        else if (operator == Operator.KW_IN) compiler.compileIn();
        else throw new CompilationError(this, "Binary operator not implemented: " + operator + "!");
    }

    public ASTBinaryOp(Operator operator, ASTExpression lhs, ASTExpression rhs) {
        super();
        this.operator = operator;
        this.lhs = lhs;
        this.rhs = rhs;
    }
}
