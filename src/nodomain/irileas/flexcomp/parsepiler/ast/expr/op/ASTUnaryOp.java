package nodomain.irileas.flexcomp.parsepiler.ast.expr.op;

import nodomain.irileas.flexcomp.backend.Backend;
import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.constant.ASTInteger;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Operator;

public class ASTUnaryOp extends ASTExpression {
    private final Operator operator;
    private final ASTExpression expression;

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        if (operator == Operator.MATHS_MINUS && expression instanceof ASTInteger) {
            new ASTInteger(-((ASTInteger) expression).getIntValue()).compile(compiler, scope);
        } else {
            expression.compile(compiler, scope);
            if (operator == Operator.LOGICAL_NOT) {
                compiler.compileLogicalNot();
            } else if (operator == Operator.KW_CHILD) {
                compiler.compileLoadChild();
            } else if (operator == Operator.KW_SIBLING) {
                compiler.compileLoadSibling();
            } else if (operator == Operator.KW_PARENT) {
                compiler.compileLoadParent();
            } else if (operator == Operator.MATHS_PLUS) {
                // DO NOTHING
            } else if (operator == Operator.MATHS_MINUS) {
                compiler.compileNegation();
            } else {
                throw new CompilationError(this, "Operator not implemented: " + operator + "!");
            }
        }
    }

    @Override
    public int precalculateValue(Backend backend) throws CompilationError {
        if (operator == Operator.MATHS_PLUS) {
            return expression.precalculateValue(backend);
        } else if (operator == Operator.MATHS_MINUS) {
            return -expression.precalculateValue(backend);
        } else {    // TODO: More operations?
            return super.precalculateValue(backend); // Error
        }
    }

    public ASTUnaryOp(Operator operator, ASTExpression expression) {
        super();
        this.operator = operator;
        this.expression = expression;
    }
}
