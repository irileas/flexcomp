package nodomain.irileas.flexcomp.parsepiler.ast.expr.objaccess;

import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Symbol;

public class ASTNew extends ASTExpression {
    private final Symbol name;
    private final ASTExpression[] params;

    public ASTNew(Symbol name, ASTExpression[] params) {
        this.name = name;
        this.params = params;
    }

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        for (ASTExpression expr : params) {
            expr.compile(compiler, scope);
            compiler.compilePushOnParamStack();
        }
        compiler.compileLoadGlobal(name);
        compiler.compileInstantiate();
        compiler.compileCallMember(compiler.getBackend().createSymbol(Symbol.get("Create")), params.length);
    }
}
