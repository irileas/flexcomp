package nodomain.irileas.flexcomp.parsepiler.ast.expr.objaccess;

import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Symbol;

public class ASTMemberAccess extends ASTExpression {
    private final ASTExpression target;
    private final Symbol key;

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        target.compile(compiler, scope);
        compiler.compileLoadMember(compiler.getBackend().createSymbol(key));
    }

    @Override
    public void compileAssignment(FunctionCompiler compiler, Scope scope) throws CompilationError {
        target.compile(compiler, scope);
        compiler.compileStoreMember(compiler.getBackend().createSymbol(key));
    }

    public ASTMemberAccess(ASTExpression target, Symbol key) {
        super();
        this.target = target;
        this.key = key;
    }
}
