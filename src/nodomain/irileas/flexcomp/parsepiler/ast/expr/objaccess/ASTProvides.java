package nodomain.irileas.flexcomp.parsepiler.ast.expr.objaccess;

import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Symbol;

public class ASTProvides extends ASTExpression {
    private final ASTExpression lhs;
    private final Symbol symbol;

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        lhs.compile(compiler, scope);
        compiler.compileProvides(compiler.getBackend().createSymbol(symbol));
    }

    public ASTProvides(ASTExpression lhs, Symbol symbol) {
        this.lhs = lhs;
        this.symbol = symbol;
    }
}
