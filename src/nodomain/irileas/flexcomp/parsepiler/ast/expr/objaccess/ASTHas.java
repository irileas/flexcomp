package nodomain.irileas.flexcomp.parsepiler.ast.expr.objaccess;

import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Symbol;

public class ASTHas extends ASTExpression {
    private final ASTExpression object;
    private final Symbol flag;

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        object.compile(compiler, scope);
        compiler.compileHasFlag(compiler.getBackend().createSymbol(flag));
    }

    public ASTHas(ASTExpression object, Symbol flag) {
        this.object = object;
        this.flag = flag;
    }
}
