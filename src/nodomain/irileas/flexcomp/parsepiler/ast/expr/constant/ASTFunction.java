package nodomain.irileas.flexcomp.parsepiler.ast.expr.constant;

import nodomain.irileas.flexcomp.backend.Backend;
import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.ast.instr.ASTBlock;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Symbol;

public class ASTFunction extends ASTExpression {
    private final Symbol[] params;
    private final ASTBlock body;

    @Override
    public int precalculateValue(Backend backend) throws CompilationError {
        // TODO: Cache the results to optimize performance?
        FunctionCompiler compiler = backend.createFunction();
        for (Symbol var : params) compiler.getScope().addVariable(var);
        body.compile(compiler, compiler.getScope());
        return compiler.finishCompilation();
    }

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        compiler.compileLoadConstant(precalculateValue(compiler.getBackend()));
    }

    public ASTFunction(Symbol[] params, ASTBlock body) {
        this.params = params;
        this.body = body;
    }
}
