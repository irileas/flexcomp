package nodomain.irileas.flexcomp.parsepiler.ast.expr.constant;

import nodomain.irileas.flexcomp.backend.Backend;
import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;

public class ASTString extends ASTExpression {
    private String value;

    @Override
    public int precalculateValue(Backend backend) throws CompilationError {
        return backend.createString(value);
    }

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        compiler.compileLoadConstant(compiler.getBackend().createString(value));
    }

    public ASTString(String value) {
        this.value = value;
    }
}
