package nodomain.irileas.flexcomp.parsepiler.ast.expr.constant;

import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;

public class ASTNil extends ASTExpression {
    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        compiler.compileLoadNil();
    }
}
