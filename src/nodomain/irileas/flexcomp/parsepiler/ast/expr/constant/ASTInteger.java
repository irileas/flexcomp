package nodomain.irileas.flexcomp.parsepiler.ast.expr.constant;

import nodomain.irileas.flexcomp.backend.Backend;
import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;

public class ASTInteger extends ASTExpression {
    private final int value;

    public int getIntValue() {
        return value;
    }

    @Override
    public int precalculateValue(Backend backend) throws CompilationError {
        return value;
    }

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        compiler.compileLoadConstant(value);
    }

    public ASTInteger(int value) {
        this.value = value;
    }
}
