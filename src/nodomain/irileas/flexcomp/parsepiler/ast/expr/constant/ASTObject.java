package nodomain.irileas.flexcomp.parsepiler.ast.expr.constant;

import nodomain.irileas.flexcomp.backend.Backend;
import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.ast.expr.ASTExpression;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Symbol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ASTObject extends ASTExpression {
    // TODO: Names
    private Map<Symbol, ASTExpression> attributes = new HashMap<>();
    private Map<Symbol, Boolean> flags = new HashMap<>();
    private List<Symbol> parents = new ArrayList<>();
    private Symbol location;

    @Override
    public int precalculateValue(Backend backend) throws CompilationError {
        int obj = backend.createObject();

        FunctionCompiler initFunc = backend.getInitFunctionCompiler();
        Scope scope = initFunc.getScope().createSubscope();

        // TODO: Don't do anything is this is a generic object
        initFunc.compileLoadConstant(obj);
        initFunc.compilePush();

        for (Symbol parent : parents) {
            initFunc.compileDup();
            initFunc.compileLoadGlobal(parent); // TODO: The parent must be initialized!
            initFunc.compileInherits();
        }

        if (location != null) {
            initFunc.compileDup();
            initFunc.compileLoadGlobal(location);
            initFunc.compileMoveTo();
        }

        for (Symbol key : attributes.keySet()) {
            int sym = backend.createSymbol(key);
            initFunc.compileDup();
            attributes.get(key).compile(initFunc, scope);
            initFunc.compileStoreMember(sym);
        }

        for (Symbol key : flags.keySet()) {
            int sym = backend.createSymbol(key);
            initFunc.compileDup();
            if (flags.get(key)) initFunc.compileGiveFlag(sym);
            else initFunc.compileGiveNegatedFlag(sym);
        }
        initFunc.compilePop();

        return obj;
    }

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        compiler.compileLoadLocal(precalculateValue(compiler.getBackend()));
    }

    public void addAttribute(Symbol attr, ASTExpression value) {
        attributes.put(attr, value);
    }

    public void addFlag(Symbol name, boolean value) {
        flags.put(name, value);
    }

    public void inherit(Symbol objectname) {
        if (!parents.contains(objectname)) parents.add(objectname);
    }

    public void locate(Symbol locationName) {
        this.location = locationName;
    }
}
