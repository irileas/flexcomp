package nodomain.irileas.flexcomp.parsepiler.ast.expr;

import nodomain.irileas.flexcomp.backend.Backend;
import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Symbol;

public class ASTSymbol extends ASTExpression {
    private final Symbol symbol;

    @Override
    public int precalculateValue(Backend backend) throws CompilationError {
        return backend.getBinding(symbol);
    }

    @Override
    public void compile(FunctionCompiler compiler, Scope scope) throws CompilationError {
        int index = scope.findVariable(symbol);
        if (index >= 0) {
            compiler.compileLoadLocal(index);
        } else {
            compiler.compileLoadGlobal(symbol);
        }
    }

    @Override
    public void compileAssignment(FunctionCompiler compiler, Scope scope) throws CompilationError {
        int index = scope.findVariable(symbol);
        if (index >= 0) {
            compiler.compileStoreLocal(index);
        } else {
            compiler.compileStoreGlobal(symbol);
        }
    }

    public ASTSymbol(Symbol symbol) {
        this.symbol = symbol;
    }
}
