package nodomain.irileas.flexcomp.driver;

import nodomain.irileas.flexcomp.backend.Backend;
import nodomain.irileas.flexcomp.parsepiler.error.CompilationError;
import nodomain.irileas.flexcomp.parsepiler.error.SyntaxError;
import nodomain.irileas.flexcomp.parsepiler.Parser;
import nodomain.irileas.flexcomp.parsepiler.ast.ASTUnit;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.Tokenizer;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Symbol;

import java.io.*;
import java.util.*;

public class CompilationDriver {
    private final File unitDir;
    private final File mainUnit;

    private File findUnitFile(Symbol id) throws FileNotFoundException {
        if (id == null) return mainUnit;
        String name = id.getName();
        // TODO: Search
        throw new FileNotFoundException("Didn't find file: \"" + name + "\"");
    }

    private ASTUnit parse(File file) throws IOException, SyntaxError {
        return new Parser(new Tokenizer(new FileInputStream(file))).parse();
    }

    private void compile(Backend backend) throws IOException, SyntaxError, CompilationError {
        HashSet<Symbol> loadedUnits = new HashSet<>();
        Queue<Symbol> toProcess = new LinkedList();
        toProcess.add(null);    // The main file

        while (!toProcess.isEmpty()) {
            Symbol name = toProcess.poll();
            if (loadedUnits.contains(name)) continue;

            File file = findUnitFile(name);
            ASTUnit unit = parse(file);
            loadedUnits.add(name);

            for (Symbol symbol : unit.getUsedUnits()) {
                toProcess.add(symbol);
            }

            unit.compile(backend);
        }
        backend.finish();
    }

    public boolean start(Backend backend) {
        try {
            compile(backend);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SyntaxError e) {
            // TODO: Better traces (e.g. print the AST and its line/file)
            System.out.println("Syntax error!");
            System.out.println(e.getMessage());
            e.printStackTrace();
        } catch (CompilationError e) {
            // TODO: Better traces (e.g. print the AST and its line/file)
            System.out.println("Compilation error!");
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    public CompilationDriver(String main) throws FileNotFoundException {
        this(new File(main));
    }

    public CompilationDriver(File main) throws FileNotFoundException {
        if (!main.exists()) throw new FileNotFoundException("Main file not found!");
        this.mainUnit = main;
        unitDir = main.getParentFile();
    }
}
