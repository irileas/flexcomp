package nodomain.irileas.flexcomp.backend;

import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Symbol;

public interface Scope {
    Scope createSubscope();
    Scope createSubscope(JumpLabel breakLabel, JumpLabel continueLabel);
    JumpLabel getBreakLabel();
    JumpLabel getContinueLabel();
    int getVarCount();
    int getAllocVarCount();
    void addVariable(Symbol name);
    int findVariable(Symbol name);
    void finish();
}
