package nodomain.irileas.flexcomp.backend;

import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Symbol;

import java.util.function.Consumer;

public interface FunctionCompiler extends Compiler {
    Scope getScope();
    JumpLabel openLabel();

    CompilationPosition getPosition();                  // Only called by JumpLabel!!!
    Consumer<CompilationPosition> compileJump();        // Only called by JumpLabel!!!
    Consumer<CompilationPosition> compileJumpIf();      // Only called by JumpLabel!!!
    Consumer<CompilationPosition> compileJumpIfNot();   // Only called by JumpLabel!!!

    void compileLoadSelf();
    void compileLoadTrue();
    void compileLoadFalse();
    void compileLoadNil();
    void compileLoadConstant(int constant);
    void compileLoadLocal(int local);
    void compileStoreLocal(int local);
    void compileLoadGlobal(Symbol symbol);
    void compileStoreGlobal(Symbol symbol);
    void compileLoadMember(int message);
    void compileStoreMember(int message);
    void compileRemoveMember(int message);
    void compileHasFlag(int symbol);
    void compileGiveFlag(int symbol);
    void compileGiveNegatedFlag(int symbol);
    void compileRemoveFlag(int symbol);
    void compileProvides(int symbol);
    void compileInherits();

    void compileCall(int params);
    void compileCallMember(int message, int params);
    void compileCallSuper(int message, int params);

    void compilePushOnParamStack();
    void compilePush();
    void compileDup();
    void compilePop();
    void compileInstantiate();
    void compileDelete();
    void compileMoveTo();
    void compileReturn();

    void compileLoadFirstObject();
    void compileLoadNextObject();
    void compileLoadChild();
    void compileLoadSibling();
    void compileLoadParent();

    void compileIs();
    void compileIn();

    void compileLogicalNot();
    void compileCheckEQ();
    void compileCheckINEQ();
    void compileCheckLESS();
    void compileCheckLEQ();
    void compileCheckGREATER();
    void compileCheckGEQ();

    void compileAddition();
    void compileSubtraction();
    void compileMultiplication();
    void compileDivision();
    void compileModulo();
    void compileNegation();
}
