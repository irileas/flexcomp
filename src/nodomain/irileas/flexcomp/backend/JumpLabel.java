package nodomain.irileas.flexcomp.backend;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class JumpLabel {
    private final FunctionCompiler compiler;
    private CompilationPosition pos = null;
    private List<Consumer<CompilationPosition>> pending = new ArrayList();

    public void compileJump() {
        if (pos == null) {
            pending.add(compiler.compileJump());
        } else {
            compiler.compileJump().accept(pos);
        }
    }

    public void compileJumpIf() {
        if (pos == null) {
            pending.add(compiler.compileJumpIf());
        } else {
            compiler.compileJumpIf().accept(pos);
        }
    }

    public void compileJumpIfNot() {
        if (pos == null) {
            pending.add(compiler.compileJumpIfNot());
        } else {
            compiler.compileJumpIfNot().accept(pos);
        }
    }

    public void place() {
        if (this.pos == null) {
            this.pos = this.compiler.getPosition();
            for (Consumer<CompilationPosition> consumer : pending) consumer.accept(pos);
            pending.clear();
        }
    }

    public JumpLabel(FunctionCompiler compiler) {   // TODO: Three parameters for
        this.compiler = compiler;
    }
}
