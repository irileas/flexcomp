package nodomain.irileas.flexcomp.backend;

import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Symbol;

import java.io.IOException;

public interface Backend {
    FunctionCompiler getInitFunctionCompiler();

    FunctionCompiler createFunction();
    int createObject();
    int createString(String text);
    int createSymbol(Symbol symbol);

    void bind(Symbol symbol, int object);
    int getBinding(Symbol symbol);

    void finish() throws IOException;
}
