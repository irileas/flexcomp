package nodomain.irileas.flexcomp.backend;

public interface Compiler {
    Backend getBackend();
    int finishCompilation();
}
