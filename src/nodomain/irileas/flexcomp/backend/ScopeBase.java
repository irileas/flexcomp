package nodomain.irileas.flexcomp.backend;

import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Symbol;

import java.util.ArrayList;
import java.util.List;

public abstract class ScopeBase implements Scope {
    private final ScopeBase parent;
    private final List<Symbol> vars = new ArrayList<>();
    private JumpLabel breakLabel = null;
    private JumpLabel continueLabel = null;
    private int maxVars;

    protected Scope getParent() {
        return parent;
    }

    public JumpLabel getBreakLabel() {
        if (breakLabel != null) return breakLabel;
        else if (getParent() != null) return parent.getBreakLabel();
        else return null;
    }

    public JumpLabel getContinueLabel() {
        if (breakLabel != null) return continueLabel;
        else if (getParent() != null) return parent.getContinueLabel();
        else return null;
    }

    public int getVarCount() {
        return ((parent == null) ? 0 : parent.getVarCount()) + vars.size();
    }

    public int getAllocVarCount() {
        return maxVars;
    }

    private void notifyNewChildSize(int size) {
        maxVars = Math.max(maxVars, size);
        if (parent != null) parent.notifyNewChildSize(maxVars);
    }

    public void addVariable(Symbol name) {
        vars.add(name);
        notifyNewChildSize(getVarCount());
    }

    public int findVariable(Symbol name) {
        int index = vars.lastIndexOf(name);
        if (index >= 0) {
            return index + ((parent == null) ? 0 : parent.getVarCount());
        } else if (parent != null) {
            return parent.findVariable(name);
        } else {
            return -1;
        }
    }

    public abstract Scope createSubscope();
    public abstract Scope createSubscope(JumpLabel breakLabel, JumpLabel continueLabel);

    protected ScopeBase(ScopeBase parent) {
        this(parent, null, null);
    }
    protected ScopeBase(ScopeBase parent, JumpLabel breakLabel, JumpLabel continueLabel) {
        this.parent = parent;
        this.breakLabel = breakLabel;
        this.continueLabel = continueLabel;
    }
}
