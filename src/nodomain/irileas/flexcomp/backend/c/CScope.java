package nodomain.irileas.flexcomp.backend.c;

import nodomain.irileas.flexcomp.backend.JumpLabel;
import nodomain.irileas.flexcomp.backend.Scope;
import nodomain.irileas.flexcomp.backend.ScopeBase;

public class CScope extends ScopeBase {

    public CScope() { this(null); }
    private CScope(CScope parent) {
        super(parent);
    }
    private CScope(CScope parent, JumpLabel breakLabel, JumpLabel continueLabel) {
        super(parent, breakLabel, continueLabel);
    }

    @Override
    public Scope createSubscope() {
        return new CScope(this);
    }

    @Override
    public Scope createSubscope(JumpLabel breakLabel, JumpLabel continueLabel) {
        return new CScope(this, breakLabel, continueLabel);
    }

    @Override
    public void finish() {
        // TODO
    }
}
