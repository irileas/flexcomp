#ifndef FLEXLIBC_H_
#define FLEXLIBC_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>


#define FLEX_TRUE ~0
#define FLEX_FALSE 0
#define FLEX_NIL 0


struct symbol {
};

struct object_slot {
    struct symbol *key;
    int value;
};

struct object_flag {
    struct symbol *key;
    bool value;
};

struct object {
    int *parents;
    struct object_slot *slots;
    struct object_flag *flags;
};

enum instance_type {
    TYPE_FUNCTION,
    TYPE_SYMBOL,
    TYPE_OBJECT,
    TYPE_STRING
};

struct instance {
    unsigned int id;
    enum instance_type type;
    union {
        int (*fptr)(int, int, unsigned int, int*);
        const char *sptr;
        struct symbol *symptr;
        struct object obody;
    } payload;
};


#define NEW_OBJECT_PAYLOAD {NULL, NULL, NULL}



inline struct instance *get_instance(int key) {
    return &FIXED_OBJECTS[key];
}

inline struct object *get_object(int key) {
    struct instance *instance;

    instance = get_instance(key);
    if (instance != NULL && instance->type == TYPE_OBJECT) {
        return &instance->obody;
    } else {
        return NULL;
    }
}


int load_attrib(int obj, int attrib) {
    struct object *object;

    object = get_object(obj);
    if (object != NULL) {
        // TODO
    } else {
        return NIL;
    }
}

int load_super_attrib(int obj, int attrib) {
}

void store_attrib(int obj, int attrib, int value) {
}

void remove_attrib(int obj, int attrib) {
}

int provides_attrib(int obj, int attrib) {
}

int has_flag(int obj, int flag) {
}

void give_flag(int obj, int flag) {
}

void give_negated_flag(int obj, int flag) {
}

void remove_flag(int obj, int flag) {
}

int inherits(int obj, int parent) {
    // TODO
    return obj;
}

int is_instance_of(int obj, int a_class) {
}

int is_in(int obj, int parent) {
}

int call(int func, int accu, int self, int param_count, int *param_stack) {
}

int instantiate_object(int object) {
}

int delete_object(int object) {
}

int move_object_to(int object, int loc) {
}

int first_object() {
}

int next_object(int object) {
}

int object_child(int object) {
}

int object_sibling(int object) {
}

int object_parent(int object) {
}


int go(int, int, unsigned int, int*);

int main(int argc, char *argv[]) {
    go(0, 0, 0, NULL);
    return 0;
}


#endif
