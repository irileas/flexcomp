package nodomain.irileas.flexcomp.backend.c;

import nodomain.irileas.flexcomp.backend.CompilationPosition;

public class CCompilationPosition implements CompilationPosition {
    private final String labelText;

    public String getLabelText() { return labelText; }

    public CCompilationPosition(String labelText) {
        this.labelText = labelText;
    }
}
