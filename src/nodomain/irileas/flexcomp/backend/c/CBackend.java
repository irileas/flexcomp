package nodomain.irileas.flexcomp.backend.c;

import nodomain.irileas.flexcomp.backend.Backend;
import nodomain.irileas.flexcomp.backend.FunctionCompiler;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Symbol;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;

public class CBackend implements Backend {
    /*
     * Requires implementation of the following functions and variables:
     *
     *  - struct object;
     *
     *  - NEW_OBJECT_PAYLOAD
     *  - int load_attrib(int obj, int attrib);
     *  - int load_super_attrib(int obj, int attrib);
     *  - void store_attrib(int obj, int attrib, int value);
     *  - void remove_attrib(int obj, int attrib);
     *  - int provides_attrib(int obj, int attrib);
     *  - int has_flag(int obj, int flag);
     *  - void give_flag(int obj, int flag);
     *  - void give_negated_flag(int obj, int flag);
     *  - void remove_flag(int obj, int flag);
     *  - int inherits(int obj, int parent);    // Returns obj
     *  - int is_instance_of(int obj, int a_class); // Returns either true or false
     *  - int is_in(int obj, int parent); // Returns either true or false
     *  - int call(int func, int accu, int self, int param_count, int* param_stack);      // Returns the result
     *  - int instantiate_object(int object);
     *  - int delete_object(int object);    // Returns a boolean, depending on success
     *  - int move_object_to(int object, int loc);  // Returns object
     *  - int first_object();
     *  - int next_object(int object);
     *  - int object_child(int object);
     *  - int object_sibling(int object);
     *  - int object_parent(int object);
     */

    private static int ID_COUNTER = 0;
    public static String newID() { return "id" + ID_COUNTER++; }

    // ---

    private final OutputStreamWriter output;
    private final List<Symbol> globals = new ArrayList<>();
    private final Map<Integer, Integer> bindings = new HashMap<>();
    private final Map<Symbol, Integer> loadedSymbols = new HashMap<>();
    private final Set<CFunctionCompiler> compilers = new HashSet();
    private final CFunctionCompiler initCompiler = new CFunctionCompiler(this, "go");
    private final List<String> functionDecls = new ArrayList<>();
    private final List<String> fixedObjectDecls = new ArrayList<>();
    private final StringBuilder stringStorage = new StringBuilder();
    private boolean finished = false;


    public CBackend(OutputStream stream) {
        this.output = new OutputStreamWriter(stream);
    }


    /*
     * C Backend-Specific Code
     */

    void writeLn(String text) throws IOException {
        output.write(text);
        output.write("\r\n");
    }

    int getPositionInGlobalsArray(Symbol sym) {
        int index = globals.indexOf(sym);
        if (index >= 0) return index;
        else {
            globals.add(sym);
            return globals.size() - 1;
        }
    }

    private int writeNewStaticObject(String type, String payloadInit) {
        int index = fixedObjectDecls.size();
        if (payloadInit.isEmpty()) {
            fixedObjectDecls.add("{ .id = " + index + ", .type = " + type + " },");
        } else {
            fixedObjectDecls.add("{ .id = " + index + ", .type = " + type + ", .payload = { " + payloadInit + " } },");
        }
        return index;
    }

    int registerFunction(String name, String code) {
        functionDecls.add(code);
        return writeNewStaticObject("TYPE_FUNCTION", ".fptr = " + name);
    }


    /*
     * Interface Methods
     */

    @Override
    public FunctionCompiler getInitFunctionCompiler() {
        return initCompiler;
    }

    @Override
    public FunctionCompiler createFunction() {
        CFunctionCompiler compiler = new CFunctionCompiler(this, newID());
        compilers.add(compiler);
        return compiler;
    }

    @Override
    public int createObject() {
        return writeNewStaticObject("TYPE_OBJECT", ".obody = NEW_OBJECT_PAYLOAD");
    }

    @Override
    public int createString(String text) {
        int pos = stringStorage.toString().getBytes().length;
        stringStorage.append(text);
        stringStorage.append("\0");
        return writeNewStaticObject("TYPE_STRING", ".sval = &STRING_BUFFER[" + pos + "]");
    }

    @Override
    public int createSymbol(Symbol symbol) {
        if (loadedSymbols.containsKey(symbol)) return loadedSymbols.get(symbol);
        int i = writeNewStaticObject("TYPE_SYMBOL", "");
        loadedSymbols.put(symbol, i);
        return i;
    }

    @Override
    public void bind(Symbol symbol, int object) {
        bindings.put(getPositionInGlobalsArray(symbol), object);
    }

    @Override
    public int getBinding(Symbol symbol) {
        return bindings.get(getPositionInGlobalsArray(symbol));
    }

    @Override
    public void finish() throws IOException {
        if (finished) return;
        finished = true;

        FunctionCompiler compiler = getInitFunctionCompiler();
        compiler.compileLoadGlobal(Symbol.get("Main"));
        compiler.compileCall(0);
        compiler.finishCompilation();

        // TODO: Write everything to output
        writeLn("const int GLOBALS_SIZE = " + globals.size() + ";");
        writeLn("const int FIXED_OBJECT_COUNT = " + fixedObjectDecls.size() + ";");
        writeLn("");
        writeLn("#include <flexlibc.h>");
        writeLn("");

        writeLn("const int GLOBALS[] = {");
        for (int x = 0; x < globals.size(); x++) {
            writeLn("    " + String.format("%10d", bindings.getOrDefault(x, -1)) + ",  /* " + globals.get(x) + " */");
        }
        writeLn("};");
        writeLn("");

        for (String s : functionDecls) writeLn(s);

        fixedObjectDecls.size();
        writeLn("struct object FIXED_OBJECTS[] = {");
        for (String s : fixedObjectDecls) writeLn("    " + s);
        writeLn("};");
        writeLn("");

        output.write("const char STRING_BUFFER[] = \"");
        try {
            byte[] bytes = stringStorage.toString().getBytes("UTF-8");
            for (int c = 0; c < bytes.length; c++) {
                if (c % 16 == 0) {
                    writeLn("\" \\");
                    output.write("    \"");
                }
                int i = bytes[c];
                if (i < 0) i += 256;
                output.write("\\0x" + String.format("%02x", i));
            }
        } catch (UnsupportedEncodingException e) {
        }
        writeLn("\";");

        output.flush();
    }
}
