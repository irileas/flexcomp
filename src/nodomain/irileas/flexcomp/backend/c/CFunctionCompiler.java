package nodomain.irileas.flexcomp.backend.c;

import nodomain.irileas.flexcomp.backend.*;
import nodomain.irileas.flexcomp.parsepiler.tokenizer.tokens.Symbol;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class CFunctionCompiler implements FunctionCompiler {
    class SourceLine {
        private String text;

        public String getText() { return text; }
        public void setText(String text) { this.text = text; }

        public SourceLine(String text) {
            this.text = text;
        }
    }

    private final CBackend backend;
    private final String name;
    private final ScopeBase scope;
    private final List<SourceLine> code;
    private boolean finished = false;
    private int CURRENT_LOCAL_STACK_SIZE = 0;
    private int MAX_LOCAL_STACK_SIZE = 0;
    private int CURRENT_STACK_SIZE = 0;
    private int MAX_STACK_SIZE = 0;


    public CFunctionCompiler(CBackend backend, String name) {
        this.backend = backend;
        this.name = name;
        this.scope = new CScope();
        this.code = new ArrayList<>();
    }

    private void changeLocalStackSize(int delta) {
        CURRENT_LOCAL_STACK_SIZE += delta;
        MAX_LOCAL_STACK_SIZE = Math.max(MAX_LOCAL_STACK_SIZE, CURRENT_LOCAL_STACK_SIZE);
    }

    private void changeStackSize(int delta) {
        CURRENT_STACK_SIZE += delta;
        MAX_STACK_SIZE = Math.max(MAX_STACK_SIZE, CURRENT_STACK_SIZE);
    }

    private SourceLine writeLn(String text) {
        SourceLine line = new SourceLine(text);
        code.add(line);
        return line;
    }

    @Override
    public ScopeBase getScope() {
        return scope;
    }

    @Override
    public JumpLabel openLabel() {
        return new JumpLabel(this);
    }

    @Override
    public CompilationPosition getPosition() {
        String label = CBackend.newID();
        writeLn("  " + label + ":");
        return new CCompilationPosition(label);
    }

    private String getParamPushString() {
        final String str = "stack[" + CURRENT_STACK_SIZE + "] = accu";
        changeStackSize(1);
        return str;
    }

    private String getPushString() {
        final String str = "local_stack[" + CURRENT_LOCAL_STACK_SIZE + "] = accu";
        changeLocalStackSize(1);
        return str;
    }

    private String getPopString() {
        changeLocalStackSize(-1);
        return "local_stack[" + CURRENT_LOCAL_STACK_SIZE + "]";
    }

    private String getDupString() {
        // TODO: Since we're storing everything in an array, don't do DUPs, but don't run the next pop() on this level!
        final String str = "local_stack[" + CURRENT_LOCAL_STACK_SIZE + "] = local_stack[" + (CURRENT_LOCAL_STACK_SIZE - 1) + "]";
        changeLocalStackSize(1);
        return str;
    }

    @Override
    public Consumer<CompilationPosition> compileJump() {
        SourceLine line = writeLn(null);
        return l -> line.setText("    goto " + ((CCompilationPosition) l).getLabelText() + ";");
    }

    @Override
    public Consumer<CompilationPosition> compileJumpIf() {
        SourceLine line = writeLn(null);
        return l -> line.setText("    if (accu) goto " + ((CCompilationPosition) l).getLabelText() + ";");
    }

    @Override
    public Consumer<CompilationPosition> compileJumpIfNot() {
        SourceLine line = writeLn(null);
        return l -> line.setText("    if (!accu) goto " + ((CCompilationPosition) l).getLabelText() + ";");
    }

    @Override
    public void compileLoadSelf() {
        writeLn("    accu = self;");
    }

    @Override
    public void compileLoadTrue() {
        writeLn("    accu = FLEX_TRUE;");
    }

    @Override
    public void compileLoadFalse() {
        writeLn("    accu = FLEX_FALSE;");
    }

    @Override
    public void compileLoadNil() {
        writeLn("    accu = FLEX_NIL;");
    }

    @Override
    public void compileLoadConstant(int constant) {
        writeLn("    accu = " + constant + ";");
    }

    @Override
    public void compileLoadLocal(int local) {
        writeLn("    accu = locals[" + local + "];");
    }

    @Override
    public void compileStoreLocal(int local) {
        writeLn("    locals[" + local + "] = accu;");
    }

    @Override
    public void compileLoadGlobal(Symbol symbol) {
        writeLn("    accu = GLOBALS[" + backend.getPositionInGlobalsArray(symbol) + "];");
    }

    @Override
    public void compileStoreGlobal(Symbol symbol) {
        writeLn("    GLOBALS[" + backend.getPositionInGlobalsArray(symbol) + "] = accu;");
    }

    @Override
    public void compileLoadMember(int message) {
        writeLn("    accu = load_attrib(accu, " + message + ");");
    }

    @Override
    public void compileStoreMember(int message) {
        writeLn("    store_attrib(accu, " + message + ", " + getPopString() + ");");
    }

    @Override
    public void compileRemoveMember(int message) {
        writeLn("    remove_attrib(accu, " + message + ");");
    }

    @Override
    public void compileHasFlag(int symbol) {
        writeLn("    accu = has_flag(accu, " + symbol + ");");
    }

    @Override
    public void compileGiveFlag(int symbol) {
        writeLn("    give_flag(accu, " + symbol + ");");
    }

    @Override
    public void compileGiveNegatedFlag(int symbol) {
        writeLn("    give_negated_flag(accu, " + symbol + ");");
    }

    @Override
    public void compileRemoveFlag(int symbol) {
        writeLn("    remove_flag(accu, " + symbol + ");");
    }

    @Override
    public void compileProvides(int symbol) {
        writeLn("    accu = provides_attrib(accu, " + symbol + ");");
    }

    @Override
    public void compileInherits() {
        writeLn("    accu = inherits(" + getPopString() + ", accu);");
    }

    @Override
    public void compileCall(int params) {
        changeStackSize(-params);
        writeLn("    accu = call(accu, 0, self, " + params + ", stack);");
    }

    @Override
    public void compileCallMember(int message, int params) {
        changeStackSize(-params);
        writeLn("    accu = call_attrib(load_super_attrib(self, " + message + "), accu, self, " + params + ", stack);");
    }

    @Override
    public void compileCallSuper(int message, int params) {
        writeLn("    accu = call(load_super_attrib(self, " + message + "), accu, self, " + params + ", stack);");
    }

    @Override
    public void compilePushOnParamStack() {
        writeLn("    " + getParamPushString() + ";");
    }

    @Override
    public void compilePush() {
        writeLn("    " + getPushString() + ";");
    }

    @Override
    public void compileDup() {
        writeLn("    " + getDupString() + ";");
    }

    @Override
    public void compilePop() {
        writeLn("    accu = " + getPopString() + ";");
    }

    @Override
    public void compileInstantiate() {
        writeLn("    accu = instantiate_object(accu);");
    }

    @Override
    public void compileDelete() {
        writeLn("    accu = delete_object(accu);");
    }

    @Override
    public void compileMoveTo() {
        writeLn("    accu = move_object_to(" + getPopString() + ", accu);");
    }

    @Override
    public void compileReturn() {
        writeLn("    return accu;");
    }

    @Override
    public void compileLoadFirstObject() {
        writeLn("    accu = first_object();");
    }

    @Override
    public void compileLoadNextObject() {
        writeLn("    accu = next_object(accu);");
    }

    @Override
    public void compileLoadChild() {
        writeLn("    accu = object_child(accu);");
    }

    @Override
    public void compileLoadSibling() {
        writeLn("    accu = object_sibling(accu);");
    }

    @Override
    public void compileLoadParent() {
        writeLn("    accu = object_parent(accu);");
    }

    @Override
    public void compileIs() {
        writeLn("    accu = is_instance_of(" + getPopString() + ", accu);");
    }

    @Override
    public void compileIn() {
        writeLn("    accu = is_in(" + getPopString() + ", accu);");
    }

    @Override
    public void compileLogicalNot() {
        writeLn("    accu = !accu;");
    }

    @Override
    public void compileCheckEQ() {
        writeLn("    accu = (" + getPopString() + " == accu);");
    }

    @Override
    public void compileCheckINEQ() {
        writeLn("    accu = (" + getPopString() + " != accu);");
    }

    @Override
    public void compileCheckLESS() {
        writeLn("    accu = (" + getPopString() + " < accu);");
    }

    @Override
    public void compileCheckLEQ() {
        writeLn("    accu = (" + getPopString() + " <= accu);");
    }

    @Override
    public void compileCheckGREATER() {
        writeLn("    accu = (" + getPopString() + " > accu);");
    }

    @Override
    public void compileCheckGEQ() {
        writeLn("    accu = (" + getPopString() + " >= accu);");
    }

    @Override
    public void compileAddition() {
        writeLn("    accu = (" + getPopString() + " + accu);");
    }

    @Override
    public void compileSubtraction() {
        writeLn("    accu = (" + getPopString() + " - accu);");
    }

    @Override
    public void compileMultiplication() {
        writeLn("    accu = (" + getPopString() + " * accu);");
    }

    @Override
    public void compileDivision() {
        writeLn("    accu = (" + getPopString() + " / accu);");
    }

    @Override
    public void compileModulo() {
        writeLn("    accu = (" + getPopString() + " % accu);");
    }

    @Override
    public void compileNegation() {
        writeLn("    accu = -accu;");
    }

    @Override
    public Backend getBackend() {
        return backend;
    }

    @Override
    public int finishCompilation() {
        StringBuilder sourcecode = new StringBuilder("static int " + name + "(int self, int accu, unsigned int params, int* param_arr)\r\n{\r\n");
        int locals = scope.getAllocVarCount();
        if (locals > 0) sourcecode.append("    int locals[" + locals + "];\r\n");
        if (MAX_LOCAL_STACK_SIZE > 0) sourcecode.append("    int local_stack[" + MAX_LOCAL_STACK_SIZE + "];\r\n");
        if (MAX_STACK_SIZE > 0) sourcecode.append("    int stack[" + MAX_STACK_SIZE + "];\r\n");
        if (locals > 0 || MAX_LOCAL_STACK_SIZE > 0 || MAX_STACK_SIZE > 0) sourcecode.append("\r\n");
        if (locals > 0) {
            String v = backend.newID();
            sourcecode.append("    for (unsigned int " + v + " = 0; (" + v + " < params) && (" + v + " < " + locals + "); " + v + "++)\r\n");
            sourcecode.append("        locals[" + v + "] = param_arr[" + v + "];\r\n");
            sourcecode.append("\r\n");
        }
        for (SourceLine line : code) {
            sourcecode.append(line.getText());
            sourcecode.append("\r\n");
        }
        sourcecode.append("    return accu;\r\n");
        sourcecode.append("}\r\n");
        return backend.registerFunction(name, sourcecode.toString());
    }
}
