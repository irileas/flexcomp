package nodomain.irileas.flexcomp;

import nodomain.irileas.flexcomp.backend.c.CBackend;
import nodomain.irileas.flexcomp.driver.CompilationDriver;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {

    /*
     * TODO:
     *  - Other operators (e.g. bitshift, binary, ...)
     *  - Printing and Reading
     *  - Improve compilation of "switch"
     *  - Error features (maybe even exceptions!)
     *    - "Hotlinking" (Variable Print = REQUIRE print / OPTIONAL print)?
     *  - Finish the backend
     *    - How to save the current state of the execution?!
     *      - Keep track of all stack datastructures (this also enables us to run a GC)
     *    - Hotswap: Same datastructures, same Backend instance, but instead of compiling to c, compile to bytecodes (which are then interpreted by C)
     *  - Operator optimization during pre-compilation
     */

    public static void main(String[] args) {
        String file;

        // TODO: Flags

        if (args.length == 0) {
            System.out.print("File to compile? ");
            Scanner scanner = new Scanner(System.in);
            file = scanner.nextLine();
        } else {
            file = args[0];
        }

        try {
            CompilationDriver driver = new CompilationDriver(file);
            driver.start(new CBackend(System.out));
        } catch (FileNotFoundException e) {
            System.out.println("File not found: \"" + file + "\"!");
        }
    }
}
