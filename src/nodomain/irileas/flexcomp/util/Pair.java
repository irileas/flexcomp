package nodomain.irileas.flexcomp.util;

public class Pair<A, B> {
    private final A first;
    private final B second;

    public final A getFirst() { return first; }
    public final B getSecond() { return second; }

    public Pair(A first, B second) {
        this.first = first;
        this.second = second;
    }
}
