# What's FlexComp anyway?

FlexComp is a pet project of mine. If everything goes well it will be a small compiler for a language designed to be used as a basis for interactive fiction games, similar to [Inform 6](https://inform-fiction.org). The compiler will convert Flex source code directly into C, which can then be compiled into an executable file by a C compiler. Adding backends for other targets - e.g. WASM, JavaScript, Scheme, etc. should and will be possible, too. There might even be support for the [MIMA](https://github.com/Garmelon/mima-tools).

The goal of this program is to provide a simple, flexible and portable programming language for the development of text adventures and other CLI applications that doesn't rely on tons of legacy code and libraries that are older than their users ^^

## Why not use Inform 6/7?

I'm a huge fan of simple, flexible, easily portable programming languages. In my opinion, Inform 7 isn't one of them. The platform is way too bloated to be used as a reliable framework, and its predecessor Inform 6 has done a good job at failing to overcome the restrictions and limitations of its original target architecture, the Z machine (just look at its implementation of arrays and I/O!), so designing a new language just seemed like a necessity.

## How about using TADS, then?

If it works out for you, go ahead and use it for your game, but to me it seems to have lost too many users after the rise of Inform 7 to survive the next decades.

## Why do you even want to write interactive fiction? That's boring.

Because I'm too lazy to deal with 3D modeling, complex game engines and so-called "scripting". It's as simple as that. Besides, interactive fiction is an extremely underrated genre. Just take a look at [Anchorhead](https://anchorhead-game.com), Dragonworld, The Hobbit and many more.
